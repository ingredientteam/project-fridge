﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_your_own_idea
{
    class Order
    {
        public int ID { get; set; }
        public double TotalPrice { get; set; }

        public List<Item> itemList = new List<Item>();

        //Constructor
        public Order(int inID, double inTotalPrice)
        {
            ID = inID;
            TotalPrice = inTotalPrice;
            itemList = new List<Item>();

        }
        
        public double GetTotalPrice()
        {
            foreach (Item element in itemList)
            {
                TotalPrice = TotalPrice + element.Price;
            }

            return TotalPrice;
        }

        public override string ToString()
        {
            return "\nOrder ID: " + ID + "\nOrder TotalPrice: " + GetTotalPrice();

        }

    }
}