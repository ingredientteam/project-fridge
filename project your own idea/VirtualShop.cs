﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace project_your_own_idea
{
    class VirtualShop
    {
        private List<Item> StoreInventory = new List<Item>();
        private const string FileName = "inventory.txt";

        public void ShowProducts()
        {
            foreach (Item product in StoreInventory)
            {
                Console.WriteLine(product.Name + "\tQuanity: " + product.Quantity + "\tPrice: " + product.Price + "\tCategory: " + product.Category);
            }
        }
        public void ReadShop()
        {
            List<string> inventoryList = File.ReadAllLines(FileName).ToList();
            for (int index = 0; index < inventoryList.Count; index++)
            {
                try
                {
                    Item currentItem = new Item();
                    currentItem.Name = inventoryList[index].Split(',')[0];
                    currentItem.Quantity = int.Parse(inventoryList[index].Split(',')[1]);
                    currentItem.Price = double.Parse(inventoryList[index].Split(',')[2]) / 100.0;
                    currentItem.Category = inventoryList[index].Split(',')[3];

                    StoreInventory.Add(currentItem);
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error while reading inventory.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Line " + (index + 1));

                    Console.ReadKey(true);
                    Environment.Exit(0);
                }
            }
        }
        public Item FindItem(string name)
        {
            foreach (Item good in StoreInventory)
            {
                if (good.Name.ToLower().CompareTo(name.ToLower()) == 0)
                {
                    return good;
                }
            }
            throw new KeyNotFoundException("Such a product does not exist");
        }

        /// <summary>
        /// Writes the current inventory to file (usually due to changes)
        /// </summary>
        private void SaveInvetory()
        {
            List<string> writeFile = new List<string>();

            foreach (Item product in StoreInventory)
            {
                writeFile.Add(product.ToString());
            }
            File.WriteAllLines(FileName, writeFile);
        }

        /// <summary>
        /// Adds an item to the inventory
        /// </summary>
        /// <param name="item">the item to add</param>
        public void AddItem(Item item)
        {
            StoreInventory.Add(item);
        }
        /// <summary>
        /// Adds an item to the list
        /// </summary>
        /// <param name="name">the name of the item</param>
        /// <param name="quantity">how much of the item should be added</param>
        /// <param name="price">how expensive the item is</param>
        /// <param name="category">category of the item</param>
        public void AddItem(string name, int quantity, double price, string category)
        {
            Item item = new Item();
            item.Name = name;
            item.Quantity = quantity;
            item.Price = price;
            item.Category = category;

            StoreInventory.Add(item);
        }

        /// <summary>
        /// Modifies the quantity of an item in the inventory
        /// </summary>
        private void ModifyQuantity(Item item, int amount)
        {
            int index = 0;
            while (index < StoreInventory.Count)
            {
                if (StoreInventory[index].Name.CompareTo(item.Name) == 0)
                {
                    StoreInventory[index].Quantity = StoreInventory[index].Quantity + amount;
                }
                index++;
            }
        }

        public void BuyItem(string itemName)
        {
            Wallet userWallet = new Wallet();
            userWallet.GetWallet();
            Item purchase = new Item();

            try
            {
                purchase = FindItem(itemName);
            }
            catch (KeyNotFoundException exceptionInfo)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exceptionInfo.Message);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey(true);

                Environment.Exit(0);
            }
            if (purchase.Quantity > 0)
            {
                if (userWallet.GetBalance() >= purchase.Price)
                {
                    userWallet.TakeFromBalance(purchase.Price);
                    ModifyQuantity(purchase, -1);
                    SaveInvetory();
                    Device fridge = new Device();
                    if (fridge.CheckExistance(purchase))
                    {
                        fridge.ModifyQuantity(purchase.Name, 1);
                        fridge.SaveFridge();
                    }
                    else
                    {
                        Device.Contents.Add(purchase);
                        fridge.SaveFridge();
                    }
                }
            }
            else
            {
                throw new OutOfStockException();
            }
        }

    }
}
