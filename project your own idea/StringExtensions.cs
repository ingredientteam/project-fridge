﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_your_own_idea
{
    static class StringExtensions
    {
        /// <summary>
        /// Gets the first bit of the string
        /// </summary>
        public static string First(this String st)
        {
            st = st.Split('=')[0].ToLower();
            return st;
        }

    }
}
