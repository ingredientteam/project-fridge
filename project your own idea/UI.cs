﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace project_your_own_idea
{
    class UI
    {
        public static void Menu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("  Welcome in Project:Fridge System\n");
            Console.ResetColor();
            Console.WriteLine("  What do you want to do?\n");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\t1) Order");
            Console.WriteLine("\t2) Add items to the fridge");
            Console.WriteLine("\t3) Remove items from your fridge");
            Console.WriteLine("\t4) Check what you have in your fridge");
            Console.WriteLine("\t5) Check your FridgeWallet status");
            Console.WriteLine("\t6) Top up the FridgeWallet");
            Console.WriteLine("\n\t0) Exit");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\n   Enter the number: ");
            Console.ResetColor();
        }
        public static void OrderMethod(VirtualShop shopKeeper)
        {
            Console.WriteLine("Available products:\n");
            shopKeeper.ShowProducts();

            Console.WriteLine("Enter the names of the products, that you want to buy.");
            Console.WriteLine("When you're done, press enter with nothing typed in.");
            string addName = "";
            do
            {
                addName = Console.ReadLine();
                try
                {
                    shopKeeper.FindItem(addName); /* If the item does not exist, this causes an exception */
                    shopKeeper.BuyItem(addName);
                }
                catch (KeyNotFoundException)
                {
                    if (addName.CompareTo("") != 0)
                    {
                        Console.WriteLine("Such an item does not exist");
                        addName = uint.MaxValue + "";
                    }
                }
                catch (OutOfStockException info)
                {
                    Console.WriteLine(info.Message);
                    addName = uint.MaxValue + "";
                }
            } while (addName.CompareTo("") != 0);
        }

    }
}
