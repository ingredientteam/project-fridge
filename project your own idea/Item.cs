﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_your_own_idea
{
    class Item
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; } 
        public string Category { get; set; }

        public Item()
        {

        }

        public Item(string name)
        {
            Name = name;
        }
        public override string ToString()
        {
            return Name + "," + Quantity + ","
                 + (Price * 100) + ","
                 + Category;
        }
    }
}