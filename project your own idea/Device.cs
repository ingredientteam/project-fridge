﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace project_your_own_idea
{
    class Device
    {
        private const string FileName = "fridge.txt";
        public static List<Item> Contents { get; set; } = new List<Item>(); /* Creates an empty fridge by default */
        /* if the above line wasn't there, then if the list is not created otherwise it would be a null reference */

        public void ReadFridge()
        {
            List<string> readFile = File.ReadAllLines(FileName).ToList();
            foreach (string line in readFile)
            {
                Contents.Add(ToItem(line));
            }
        }

        public static void ShowFridge()
        {
            Device fridge = new Device();
            fridge.ReadFridge();
            foreach (Item entry in Contents)
            {
                Console.WriteLine(entry.Name + "\tQ: " + entry.Quantity + "\tP: " + entry.Price + "\tCat: " + entry.Category);
            }
        }

        public void Remove(string name)
        {
            bool removed = false;
            for (int index = 0; (index < Contents.Count) && (removed == false); index++)
            {
                if (Contents[index].Name.ToLower().CompareTo(name.ToLower()) == 0)
                {
                    Contents.RemoveAt(index);
                    removed = true;
                }
            }
            if (removed == false)
            {
                throw new KeyNotFoundException("You don't own this item");
            }
        }

        /// <summary>
        /// Modifies the quantity of an existing item
        /// </summary>
        /// <param name="name">item name</param>
        /// <param name="modifier">change of quanity</param>
        public void ModifyQuantity(string name, int modifier)
        {
            foreach (Item product in Contents)
            {
                if (product.Name.ToLower().CompareTo(name.ToLower()) == 0)
                {
                    product.Quantity = product.Quantity + modifier;
                }
            }
        }

        /// <summary>
        /// Saves the fridge contents to file
        /// </summary>
        public void SaveFridge()
        {
            StreamWriter writer = new StreamWriter(FileName);
            foreach (Item product in Contents)
            {
                string write = product.Name + "," + product.Quantity + "," + (product.Price * 100) + "," + product.Category;
                writer.WriteLine(write);
            }
            writer.Close();
        }

        /// <summary>
        /// Checks if an item exists in the list of items
        /// </summary>
        public bool CheckExistance(Item item)
        {
            foreach (Item product in Contents)
            {
                if (product.Name.ToLower().CompareTo(item.Name.ToLower()) == 0) 
                { /* Checks without case-sensitivity */
                    return true;
                }
            }
            return false;
        }
        private Item ToItem(string line)
        {
            if (line.CompareTo("") != 0)
            {
                Item add = new Item();
                string[] lineSplit = line.Split(',');
                add.Name = lineSplit[0];
                add.Quantity = int.Parse(lineSplit[1]);
                add.Price = double.Parse(lineSplit[2]);
                add.Category = lineSplit[3];

                return add;
            }
            else
            {
                throw new FormatException("Syntax error"); /* Should be caught wherever ReadFridge is used */
            }
        }
    }
}
