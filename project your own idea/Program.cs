﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_your_own_idea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode; /* Enables unicode */
            Program myProgram = new Program();
            myProgram.Run();
        }

        public void Run()
        {

            VirtualShop shopKeeper = new VirtualShop();
            Device fridge = new Device();
            shopKeeper.ReadShop();
            int caseSwitch = int.MinValue;

            do
            {
                Console.Clear();
                UI.Menu();
                try
                {
                    caseSwitch = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("The system is busy right now. Please try again.");
                    this.Run();
                    Environment.Exit(0); /* Just leave this line be, don't ask */
                }
                switch (caseSwitch)
                {
                    case 1:
                        // order
                        UI.OrderMethod(shopKeeper);
                        break;
                    case 2:
                        // add items
                        try
                        {
                            fridge.ReadFridge();
                        }
                        catch (FormatException info)
                        {
                            Console.WriteLine(info.Message);
                            Console.ReadKey(true);
                            Environment.Exit(0);
                        }
                        Item newItem = new Item();
                        Console.Write("Name: ");
                        newItem.Name = Console.ReadLine();
                        Console.Write("Quantity: ");
                        newItem.Quantity = int.Parse(Console.ReadLine());
                        Console.Write("Price (ONLY in øre): ");
                        newItem.Price = double.Parse(Console.ReadLine()) / 100;
                        Console.Write("Category: ");
                        newItem.Category = Console.ReadLine();
                        if (fridge.CheckExistance(newItem) == false)
                        {
                            Device.Contents.Add(newItem);
                            Console.WriteLine("Added");
                            fridge.SaveFridge();
                        }
                        else
                        {
                            Console.WriteLine("Item already exists");
                            Console.ReadKey(true);
                        }

                        break;
                    case 3:
                        // remove items
                        string name = "";
                        try
                        {
                            fridge.Remove(name);
                        }
                        catch (KeyNotFoundException info)
                        {
                            Console.WriteLine(info.Message);
                            Console.ReadKey(true);
                            break;
                        } 
                        fridge.SaveFridge();
                        Console.WriteLine("Deleted");
                        break;
                    case 4:
                        Device.ShowFridge();
                        Console.ReadKey(true);
                        break;
                    case 5:
                        // check FridgeWallet
                        Wallet balanceInfo = new Wallet();
                        balanceInfo.GetWallet();
                        Console.WriteLine("Your balance is: " + balanceInfo.GetBalance());
                        Console.ReadKey(true);
                        break;
                    case 6:
                        // top up FridgeWallet
                        Wallet topUp = new Wallet();
                        topUp.GetWallet();
                        Console.Write("How much do you want to add to your fridge wallet (don't cheat!): ");
                        try
                        {
                            double add = double.Parse(Console.ReadLine());
                            topUp.AddToBalance(add);
                            topUp.SaveWallet();
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Try again later");
                        }
                        break;
                    default:
                        Console.WriteLine("Wrong input.");
                        break;
                }
            } while (caseSwitch != 0);
            //catch (OutOfStockException exceptionInfo)
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.Beep(420, 100);
            //    Console.WriteLine(exceptionInfo.Message, ((Console.WindowWidth / 2) - (exceptionInfo.Message.Length / 2)));
            //    Console.ForegroundColor = ConsoleColor.White;
            //    Console.ReadKey(true);
            //}
        }
    }
}

