﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace project_your_own_idea
{
    public class OutOfStockException : Exception
    {

        public OutOfStockException() : base()
        {

        }
        public override string Message
        {
            get
            {
                return "The selected item is out of stock.";
            }
        }

    }
}
