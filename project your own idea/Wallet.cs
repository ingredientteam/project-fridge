﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace project_your_own_idea
{
    class Wallet
    {
        public string CardHolderName { get; set; } = "John Doe";
        public string CardNumber { get; set; } = "4206 9420 6942 0694";
        private int CvvNumber { get; set; } = 420;
        private string ExpirationDate { get; set; }

        private double Balance { get; set; } = 0;


        public void TopUp(double add)
        {
            Balance = Balance + add;
        }

        /// <summary>
        /// Gets the wallet from the wallet.txt file and writes it into the object
        /// </summary>
        public void GetWallet()
        {
            if (File.Exists("wallet.txt") == false)
            {
                throw new FileNotFoundException("The wallet information file cannot be found in the target directory");
            }
            List<string> walletFile = File.ReadAllLines("wallet.txt").ToList();

            foreach (string line in walletFile)
            {
                char[] splitter = new char[1];
                splitter[0] = '='; /* FUCK C# honestly */

                if (line.First().CompareTo("cardholdername") == 0)
                {
                    CardHolderName = line.Split(splitter, 2)[1];
                }
                else if (line.First().CompareTo("cardnumber") == 0)
                {
                    CardNumber = line.Split(splitter, 2)[1].Trim(' ');
                }
                else if (line.First().CompareTo("cvvnumber") == 0)
                {
                    CvvNumber = int.Parse(line.Split(splitter, 2)[1]);
                }
                else if (line.First().CompareTo("expirationdate") == 0)
                {
                    ExpirationDate = line.Split(splitter, 2)[1];
                }
                else if (line.First().CompareTo("balance") == 0)
                {
                    Balance = int.Parse(line.Split(splitter, 2)[1]) / 100.0;
                }
            }
        }

        public void SaveWallet()
        {
            List<string> fileToWrite = new List<string>();
            fileToWrite.Add("CardHolderName=" + CardHolderName);
            fileToWrite.Add("CardNumber=" + CardNumber);
            fileToWrite.Add("CVVNumber=" + CvvNumber);
            fileToWrite.Add("ExpirationDate=" + ExpirationDate);
            fileToWrite.Add("Balance=" + Balance * 100);

            File.WriteAllLines("wallet.txt", fileToWrite);
        }

        public void AddToBalance(double amount)
        {
            Balance = Balance + amount;
        }

        /// <summary>
        /// Checks if the holder has enough money in their account, then deducts money
        /// </summary>
        /// <returns>Success</returns>
        public bool TakeFromBalance(double amount)
        {
            if (GetBalance() >= amount)
            {
                DeductBalance(amount);
                SaveWallet();
                return true;
            }
            else
            {
                return false;
            }
        }
        private void DeductBalance(double amount)
        {
            Balance = Math.Round(Balance - amount, 2);
        }

        public double GetBalance()
        {
            return Balance;
        }
        public override string ToString()
        {
            return "\nWallet cardHolderName: " + CardHolderName + "\nWallet cardNumber: " + CardNumber + "\nWallet cvvNumber: " + CvvNumber + "\nWallet experationDate: " + ExpirationDate + "\nWallet balance: " + Balance;
        }

    }
}